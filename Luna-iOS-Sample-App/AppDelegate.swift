//
//  AppDelegate.swift
//  Luna-iOS-Sample-App
//
//  Created by Sebastian Rodykow on 30/12/2022.
//

import UIKit
import LunaSDK
import CoreLocation

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let lunaGateway = Luna.gateway()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        CLLocationManager().requestAlwaysAuthorization()
        startLunaGateway()
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func startLunaGateway() {
      lunaGateway.start()
    }

    func stopLunaGateway() {
      lunaGateway.stop()
    }

}

