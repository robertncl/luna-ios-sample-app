
## Building the code
1. Clone repository
```bash
  git clone git@gitlab.com:luna-xio/public/luna-ios-sample-app.git
```
2. Go into project directory
```bash
  cd Luna-iOS-Sample-App
```
3. Install required Cocoapod version by [bundler](https://bundler.io)
```bash
  bundle install
```

4. Install all required dependecies
```bash
  bundle exec pod install
```
5. Open `Luna-iOS-Sample-App.xcworkspace` in Xcode.

5. Run the project